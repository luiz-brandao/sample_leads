<?php
// database configuration parameters
$DB_HOST = $_ENV['DB_HOST'];
$DB_NAME = $_ENV['DB_NAME'];
$DB_USER = $_ENV['DB_USER'];
$DB_PASS = $_ENV['DB_PASS'];

// framework and database configuration
return [
    'displayErrorDetails' => true,
    'determineRouteBeforeAppMiddleware' => true,
    'db' => [
        'connection_string' => "mysql:host=$DB_HOST;dbname=$DB_NAME",
        'username' => $DB_USER,
        'password' => $DB_PASS,
        'return_result_sets' => true,
        'driver_options' => [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']
    ]
];