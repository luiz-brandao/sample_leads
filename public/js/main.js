/**
 * General functions
 */
$(function(){
    Vue.filter('parseInt', function (value) {
        var int = parseInt(value);

        // check if it's NaN
        return int !== int ? 0 : int;
    });

    $('.date').datetimepicker({
       timepicker:false,
       format: "Y-m-d"
    });
});

/**
 * Translates a disposition code to text
 * @param type
 * @returns {*}
 */
function translateDisposition(type){
    switch (type) {
        case 'new':
            return 'New';
        case 'open':
            return 'Open';
        case 'booked':
            return 'Booked';
        case 'booked_quote':
            return 'Booked Quote';
        case 'booked_email':
            return 'Booked with Email';
        case 'booked_upsell':
            return 'Booked with Upsell';
        case 'inbound_booked':
            return 'Inbound Booked';
        case 'inbound_declined':
            return 'Inbound Declined';
        case 'declined':
            return 'Declined';
        case 'closed':
            return 'Closed';
        case 'invalid':
            return 'Invalid';
    }
}

/**
 * List of colors for each disposition
 * 
 * @type object
 */
var colors = {
    'new': '#21ba45',
    'open': '#00b5ad',
    'booked': '#1678c2',
    'booked_quote': '#5829bb',
    'booked_email': '#a333c8',
    'booked_upsell': '#e03997',
    'inbound_booked': '#fbbd08',
    'inbound_declined': '#f2711c',
    'declined': '#db2828',
    'closed': '#a5673f',
    'invalid': '#767676',
};

