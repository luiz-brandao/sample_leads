// Vue.js view model
var vm;

// global var that holds the schema of a lead
var leadSchema = {
    info: {
        id: '',
        client_name: '',
        client_email: '',
        client_phone: '',
        client_mobile_phone: '',
        client_address: '',
        client_city: '',
        client_postal_code: '',
        client_service_date: '',
        client_comments: '',
        source: '',
        services_requested: {
            'carpet_dry_cleaning': null,
            'upholstery_cleaning': null,
            'mattress_cleaning': null,
            'tile_grout_cleaning': null,
            'air_conditioner_cleaning': null,
            'mold_cleaning': null,
            'others': null
        }
    },
    dispositions: []
};

// global var that holds the schema of a disposition
var dispositionSchema = {
    type: '',
    contact_date: null,
    booking_date: null,
    decline_reasons: null,
    open_reasons: null,
    notes: null
};

// filter to remove the date from a datetime string
Vue.filter('sliceDate', function (dateString) {
    if (dateString) {
        return dateString.slice(0, 10);
    }
});

// shortcut to deal with the modal
function leadModal(action) {
    $('#lead-modal').modal(action);
}

// code to be called on load
$(function () {
    // ajax endpoints
    var leadsDefaultUrl = '/leads/new';
    if(isArchive){
        leadsDefaultUrl += '/archive';
    }

    // init datatable component
    var table = $('#leads').DataTable({
        "lengthMenu": [10, 20, 50, 100],
        "pageLength": 20,
        "processing": true,
        "serverSide": true,
        "ajax": leadsDefaultUrl,
        "columnDefs": [
            { "searchable": true }
        ]
    });

    // processed by and disposition initially hidden
    table.column(5).visible(false);
    table.column(6).visible(false);

    // initialize lead modal
    $('.ui.modal').modal({
        observeChanges: true,
        onHide: function () {
            vm.unlockCurrentLead();
        }
    });

    // initiate accordion component
    $('.ui.accordion').accordion({
        onChange: function () {
            // when accordion expands, modal size changes
            leadModal('refresh');
        }
    });

    // handle clicks on a datatable row
    $("#leads tbody").on('click', 'tr', function (event) {
        // call function in the Vue view model
        vm.getLead($(this)[0].id);
    });

    // form validation
    $("#lead-dialog-form").form({
        fields: {
            'client_name': 'empty',
            'source': 'empty',
            'client_postal_code': 'empty',
            'client_phone': 'empty'
        }
    });

    // Vue view model
    vm = new Vue({
        el: 'body',
        data: {
            dispositionTypes: ['new', 'booked', 'booked_quote', 'booked_email', 'booked_upsell', 'declined', 'open', 'inbound_booked', 'inbound_declined', 'closed', 'invalid'],
            bookedTypes: ['booked', 'booked_quote', 'booked_email', 'booked_upsell', 'inbound_booked'],
            declinedTypes: ['declined', 'inbound_declined'],
            openTypes: ['answering_machine', 'no_answer', 'busy', 'unavailable', 'undecided'],
            lead: $.extend(true, {}, leadSchema), // current lead
            newDisposition: $.extend(true, {}, dispositionSchema), // new disposition to add
            maxDispositions: 3,
            dispositionFilter: "new", // current filter
            overview: {}, // overview of total of leads for each disposition
            user: '',
            leadColor: 'green'
        },
        computed: {
            canUpdateDisposition: function () {
                var stillOpen = ['new', 'open'].indexOf(this.lead.info.current_disposition_type) > -1;
                return !this.lead.dispositions.length || stillOpen;
            },
            isAdmin: function () {
                return this.user.is_admin;
            }
        },
        ready: function () {
            this.$http.get('/session').then(function (response) {
                this.user = response.data;
            });

            this.updateOverview(false);
        },
        methods: {
            updateOverview: function (refresh) {
                var url = '/leads/overview';

                if(isArchive){
                    url += '/archive';
                }

                this.$http.get(url).then(function (response) {
                    this.overview = response.data.overview;
                });

                if(refresh) {
                    this.showLeads(this.dispositionFilter);
                }
            },

            canEditCurrentLead: function () {
                return !this.lead.info.whos_is_editing || this.lead.info.whos_is_editing == this.user.id ;
            },

            getLead: function (leadId) {
                this.$http.get('/lead/' + leadId).then(function (response) {
                    this.lead.info = response.data.lead;
                    this.lead.dispositions = response.data.dispositions;

                    leadModal('show');
                });
            },

            unlockCurrentLead: function () {
                this.$http.get('/lead/unlock/' + this.lead.info.id).then(function (response) {

                });
            },

            checkRequiredFields: function(){
                // first check for the form validation
                if(!$("#lead-dialog-form").form("validate form")){
                    return false;
                }

                // check for disposition required fields
                if(this.newDisposition.type.match(/booked/) !== null && !this.newDisposition.booking_date){
                    alert("Select the Booking Date!");
                    return false;
                }

                if(this.newDisposition.type.match(/open/) !== null && !this.newDisposition.open_reasons){
                    alert("Select the reason!");
                    return false;
                }

                if(this.newDisposition.type.match(/declined/) !== null && !this.newDisposition.decline_reasons){
                    alert("Select the reason!");
                    return false;
                }

                return true;
            },

            updateLead: function () {
                if(!this.checkRequiredFields()){
                    return false;
                }

                this.$http.patch('/lead/' + this.lead.info.id, {
                    lead: this.lead.info,
                    newDisposition: this.newDisposition
                }).then(function (response) {
                    this.updateOverview(true);
                    leadModal('hide');
                    this.clearNewDisposition();
                });
            },

            rectifyDisposition: function (data) {
                this.$http.patch('/disposition/' + data.id, {
                    disposition: data,
                }).then(function (response) {
                    this.updateOverview(true);
                    leadModal('hide');
                    this.clearNewDisposition();
                });
            },

            clearNewDisposition: function () {
                this.newDisposition = $.extend(true, {}, dispositionSchema)
            },

            clearLead: function () {
                // we need to update the global object otherwise Vuejs won't be in sync
                var cleanLead = $.extend(true, {}, leadSchema);
                this.lead.info = cleanLead.info;
                this.lead.dispositions = cleanLead.dispositions;
            },

            openNewLead: function () {
                this.clearLead();
                this.clearNewDisposition();
                leadModal('show');
            },

            saveNewLead: function () {
                if(!this.checkRequiredFields()){
                    return false;
                }
                
                this.$http.post('/lead', {
                    lead: this.lead.info,
                    newDisposition: this.newDisposition
                }).then(function (response) {
                    this.updateOverview(true);
                    leadModal('hide');
                    this.clearNewDisposition();
                });
            },

            deleteCurrentLead: function(){
                if(!confirm("Are you sure you want to delete this lead?")){
                    return;
                }
                
                this.$http.delete('/lead/' + this.lead.info.id, {

                }).then(function (response) {
                    this.updateOverview(true);
                    leadModal('hide');
                    this.clearNewDisposition();
                });
            },

            showLeads: function (disposition) {
                this.dispositionFilter = disposition;

                var url = '/leads/' + disposition;
                if(isArchive){
                    url += '/archive';
                }

                $('#leads').DataTable().ajax.url(url).load();
                if (disposition !== 'new') table.column(6).visible(true);
            },

            filterIs: function (filter) {
                return this.dispositionFilter == filter;
            },

            closeModal: function () {
                leadModal('hide');
            }
        }
    });

});

