// Vue.js view model
var reportsVm;

$(function () {
    // Vue.js view model
    reportsVm = new Vue({
        el: 'body',
        data: {
            user: '',
            users: [],
            report: {
                report_on_user: '',
                start_date: '',
                end_date: ''
            },
            report_results: null
        },
        ready: function () {
            this.$http.get('/session').then(function (response) {
                this.user = response.data;
            });

            this.getUsers();
        },
        methods: {
            backgroundStyle: function(percentage){
                return "background: linear-gradient(90deg, rgba(33, 133, 208, 0.1) "+ percentage +"%, #FFF 50%)";
            },
            getUsers: function () {
                this.$http.get('/users').then(function (response) {
                    this.users = response.data;
                });
            },

            runReport: function () {
                this.$http.get('/reports', this.report).then(function (response) {
                    this.report_results = response.data;
                });
            },

            downloadLeadsReport: function(){
                window.location.href = '/reports/excel?report_on_user=' + this.report.report_on_user
                                        +'&start_date=' + this.report.start_date
                                        + '&end_date=' + this.report.end_date
            },

            translateDisposition: function (code) {
                return translateDisposition(code);
            }
        }
    });
});