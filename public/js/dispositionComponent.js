/**
 * Vue.js component to filter dispositions
 * It appears on the left side of the dashboard
 */
Vue.component('item-filter', {

    template: `<a class="item" :class="[color, { active: isActive }]" @click="showLeads"><slot></slot> <div class="ui label" :class="[color]">{{ total }}</div>
                </a>`,
    props: ['type', 'color'],
    computed: {
        total: function(){
            var typeTotal = this.$parent.overview[this.type];
            return typeTotal ? typeTotal : 0;
        },
        isActive: function(){
            return this.$parent.filterIs(this.type);
        }
    },
    methods: {
        showLeads: function(){
            this.$parent.showLeads(this.type);
            this.$parent.leadColor = this.color;
        }
    }

});

/**
 * Vue.js component to display dispositions widget
 * It appears on every lead modal
 */
Vue.component('lead-disposition', {
    template: `<div class="title" :class="{ active: active }">
                <i class="dropdown icon"></i>
                Contact #{{contactNum}}  <span class="by" v-if="by">by {{ by }}</span>
                <div style="float: right">
                <template v-if="disposition">
                    {{ translateDisposition(disposition) }}
                </template>                 
                <i class="icon calendar"></i> {{ contactDate ? contactDate : new Date().toLocaleString()  }}</div>
            </div>
            <div class="content" :class="{ active: active }">
                <div class="ui small form">
                    <div class="ui two column grid">
                        
                        <div class="column">
                            <div class="inline field">
                                <label>Disposition: </label>
                                <select  class="ui reason dropdown" v-model="disposition" :disabled="disabled">
                                    <!--<option value="new">New</option>-->
                                    <option value="open" v-if="canKeepOpen">Open</option>
                                    <option value="booked">Booked</option>
                                    <option value="booked_quote">Booked Quote</option>
                                    <option value="booked_email">Booked via Email</option>
                                    <option value="booked_upsell">Booked with Upsell</option>
                                    <option value="inbound_booked">Inbound Booked</option>
                                    <option value="inbound_declined">Inbound Declined</option>
                                    <option value="declined">Declined</option>
                                    <option value="closed">Closed</option>
                                    <option value="invalid">Invalid</option>
                                </select>
                            </div>
                        </div>
                        
                        <div v-if="disposition == 'open'" class="right aligned column">
                            <div class="right inline field">
                                <label>Open reason:</label>
                                <select  class="ui reason dropdown" v-model="openReasons"  :disabled="disabled">                                    
                                    <option value="answering_machine">Answering machine</option>
                                     <option value="no_answer">No answer</option>
                                     <option value="busy">Busy</option>
                                     <option value="unavailable">Contact unavailable</option>
                                     <option value="undecided">Undecided</option>
                                </select>
                            </div>
                        </div>


                        <div v-if="isDeclinedType(disposition)" class="column">
                            <div class="right inline field">
                                <label>Declining reason:</label>
                                <select  class="ui reason dropdown" v-model="declineReasons"  :disabled="disabled">                                    
                                    <option value="availability_conflict">Availability conflict</option>
                                     <option value="hung_up">Hung up</option>
                                     <option value="price_too_high">Price too high</option>
                                     <option value="shopping_around">Shopping around</option>
                                     <option value="technical_concern">Technical concern</option>
                                     <option value="went_to_competitor">Went to competitor</option>
                                    <option value="previous_open_lead">Previous open lead</option>
                                     <option value="others">Others</option>
                                </select>
                            </div>
                        </div>

                        <div v-if="isBookedType(disposition)" class="column">
                            <div class="right inline field">
                                <label>Booking Date:</label>                                
                                <input class="date" name="first-name" v-model="bookingDate"  :disabled="disabled">
                            </div>
                        </div>

                    </div>
                    <textarea style="height: 8em" v-model="notes"  :disabled="disabled">{{ comments }}</textarea>
                    <button v-if="canRectify" @click="rectify" class="ui mini orange rectify button"><i class="icon warning sign"></i> Rectify disposition</button>                                        
                </div>
            </div>`,
    props: ['id','contactNum', 'contactDate', 'bookingDate', 'declineReasons', 'openReasons', 'disposition', 'notes', 'active', 'disabled', 'by', 'isNew'],
    data: function () {
        return {
            dispositionTypes: ['new', 'booked', 'booked_quote', 'booked_email', 'booked_upsell', 'declined', 'open', 'inbound_booked', 'inbound_declined', 'closed', 'invalid'],
            bookedTypes: ['booked', 'booked_quote', 'booked_email', 'booked_upsell', 'inbound_booked'],
            declinedTypes: ['declined', 'inbound_declined'],
            openTypes: ['answering_machine','no_answer','busy','unavailable','undecided'],
        }
    },
    computed: {
        canKeepOpen: function(){
            return this.contactNum < 3;
        },
        canRectify: function(){
            return !this.isNew && this.$parent.isAdmin;
        }
    },
    ready: function(){
        $('.date').datetimepicker({
            timepicker:false,
            format: "Y-m-d"
        });
    },
    watch: {
        'disposition': function(val, oldVal){
            $('.date').datetimepicker({
                timepicker:false,
                format: "Y-m-d"
            });
        }
    },

    methods: {

        isBookedType: function (dispositionType) {
            return this.bookedTypes.indexOf(dispositionType) > -1;
        },
        isDeclinedType: function (dispositionType) {
            return this.declinedTypes.indexOf(dispositionType) > -1;
        },
        translateDisposition: function(code) {
            return translateDisposition(code);
        },
        rectify: function(){
            var data = {
                id: this.id,
                type: this.disposition,
                booking_date: this.bookingDate,
                decline_reasons: this.declineReasons,
                open_reasons: this.openReasons,
                notes: this.notes,
            }
            this.$parent.rectifyDisposition(data);
        }
    }
});