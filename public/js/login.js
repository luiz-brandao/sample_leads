/**
 * Responsible for dealing with the login form
 */
$(function(){
    new Vue({
        el: 'body',
        data: {
            username: '',
            password: '',
            dashboardPath: '/dashboard',
            loginStatus: null
        },
        methods: {
            login: function(){
                var data = {
                    username: this.username,
                    password: this.password,
                };

                this.$http.post('/login', data).then(function(response){
                    if(response.ok){
                        this.loginStatus = 'success';
                        window.location.href = this.dashboardPath;
                    }
                }, function(res){
                    this.loginStatus = 'error';
                });
            }
        }
    });
});