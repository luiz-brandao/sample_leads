// Vue.js view model
var userVm;

var newUserSchema = {
    id: '',
    name: '',
    username: '',
    email: '',
    password: '',
    is_admin: '',
};

$(function () {
    // Vue.js view model
    userVm = new Vue({
        el: 'body',
        data: {
            user: '',
            users: [],
            action: '',
            editUser: {
                id: '',
                name: '',
                username: '',
                email: '',
                password: '',
                is_admin: '',
            },
            newPassword: ''
        },
        ready: function () {
            this.$http.get('/session').then(function (response) {
                this.user = response.data;
            });

            this.editUser = Object.assign({}, newUserSchema);
            this.getUsers();
        },
        methods: {
            getUsers: function () {
                this.$http.get('/users').then(function (response) {
                    this.users = response.data;
                });
            },

            showEditUser: function (index) {
                this.action = 'edit';
                this.editUser = Object.assign({}, this.users[index]);
                $("#edit-user-modal").modal("show");
            },

            showNewUser: function () {
                this.action = 'new';
                this.editUser = Object.assign({}, newUserSchema);
                $("#edit-user-modal").modal("show");
            },

            saveUser: function () {
                if (this.action == 'edit') {
                    this.$http.patch('/user/'+this.editUser.id, this.editUser).then(function (response) {
                        this.getUsers();
                        this.closeModal();
                    }, function(response){
                        alert(response.data.errorInfo);
                    });
                }

                if (this.action == 'new') {
                    this.$http.post('/user', this.editUser).then(function (response) {
                        this.getUsers();
                        this.closeModal();
                    }, function(response){
                        alert(response.data.errorInfo);
                    });
                }
            },

            updatePassword: function(){
                this.$http.post('/user/'+this.user.id+'/password', {
                    password: this.newPassword
                }).then(function (response) {
                    window.location.href = '/dashboard';
                }, function(response){
                    alert(response.data.errorInfo);
                });
            },

            disableUser: function(){
                this.editUser.is_active = false;
                this.editUser.password = Date.now();

                this.saveUser();
            },

            enableUser: function(){
                this.editUser.is_active = true;

                this.saveUser();
            },

            closeModal: function(){
                $("#edit-user-modal").modal("hide");
            }
        }
    });
});