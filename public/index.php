<?php
session_start();

date_default_timezone_set('Australia/Sydney');

define('APP_PATH','../');
define('LOGIN_URL','/login');

$loader = require APP_PATH.'/vendor/autoload.php';
$loader->add('Edry\\', '../');

$dotenv = new Dotenv\Dotenv('../');
$dotenv->load();

$config = require APP_PATH.'/config.php';

$app = new \Slim\App(["settings" => $config]);

require APP_PATH.'/services.php';
require APP_PATH.'/middlewares.php';
require APP_PATH.'/routes.php';


$app->run();