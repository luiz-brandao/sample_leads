<div>
    <h4 class="ui dividing header">Lead #{{ lead.info.id }}</h4>


    <div v-if="!canEditCurrentLead()" class="ui icon error message">
        <i class="warning sign icon"></i>
        <div class="content">
            <div class="header">
                Lead blocked
            </div>
            <p>This lead is currently open by another user</p>
        </div>
    </div>

    <div id="lead-dialog-form">
        <div class="ui two column grid">
            <div class="column">
                <div class="ui small form">
                    <div class="inline field">
                        <label class="fixed">Name</label>
                        <input type="text" name="client_name" v-model="lead.info.client_name">
                    </div>
                    <div class="inline field">
                        <label class="fixed">Email</label>
                        <input type="text" name="client_email" v-model="lead.info.client_email">
                    </div>
                    <div class="inline field">
                        <label class="fixed">Phone</label>
                        <input type="text" name="client_phone" v-model="lead.info.client_phone">
                    </div>
                    <div class="inline field">
                        <label class="fixed">Mobile</label>
                        <input type="text" name="client_mobile_phone" v-model="lead.info.client_mobile_phone">
                    </div>


                </div>
            </div>
            <div class="column" style="text-align: right">

                <div class="ui small form">
                    <div class="inline field">
                        <label class="fixed">Address</label>
                        <input type="text" name="client_address" v-model="lead.info.client_address">
                    </div>
                    <div class="inline field">
                        <label class="fixed">City</label>
                        <input type="text" name="client_city" v-model="lead.info.client_city">
                    </div>
                    <div class="inline field">
                        <label class="fixed">Postal Code</label>
                        <input type="text" name="client_postal_code" v-model="lead.info.client_postal_code">
                    </div>
                    <div class="inline field">
                        <label class="fixed">Date of service</label>
                        <input class="date" name="client_service_date" v-model="lead.info.client_service_date">
                    </div>
                </div>

            </div>
        </div>
        <div class="ui divider"></div>
        <div class="ui two column grid">
            <div class="column">
                <div class="ui small form">
                    <div class="inline field">
                        <label class="fixed">Source</label>
                        <select class="ui dropdown" v-model="lead.info.source" name="source">
                            <option value="edry">Edry</option>
                            <option value="electrodry">Electrodry</option>
                            <option value="yellowpages">YellowPages</option>
                            <option value="email_marketing">Email Marketing</option>
                            <option value="live_chat">LiveChat</option>
                            <option value="home_improvement">HI Pages</option>
                            <option value="others">Others</option>
                        </select>
                    </div>
                    <div class="inline field" style="float: left">
                        <label class="fixed">Services</label>
                    </div>
                    <div class="services">
                        <div class="ui checkbox">
                            <input type="checkbox" v-model="lead.info.services_requested.carpet_dry_cleaning">
                            <label>Carpet Dry Cleaning</label>
                        </div>
                        <div class="ui checkbox">
                            <input type="checkbox" v-model="lead.info.services_requested.upholstery_cleaning">
                            <label>Upholstery Cleaning</label>
                        </div>
                        <div class="ui checkbox">
                            <input type="checkbox" v-model="lead.info.services_requested.mattress_cleaning">
                            <label>Mattress Cleaning</label>
                        </div>
                        <div class="ui checkbox">
                            <input type="checkbox" v-model="lead.info.services_requested.tile_grout_cleaning">
                            <label>Tile & Grout Cleaning</label>
                        </div>
                        <div class="ui checkbox">
                            <input type="checkbox" v-model="lead.info.services_requested.air_conditioner_cleaning">
                            <label>Air Conditioner Cleaning</label>
                        </div>
                        <div class="ui checkbox">
                            <input type="checkbox" v-model="lead.info.services_requested.mold_cleaning">
                            <label>Mold cleaning</label>
                        </div>
                        <div class="ui checkbox">
                            <input type="checkbox" v-model="lead.info.services_requested.others">
                            <label>Others</label>
                        </div>
                    </div>

                </div>
            </div>
            <div class="column">
                <div class="ui small form">
                    <div class="field">
                        <label style="width: auto; text-align: left">Client Comments</label>
                        <textarea name="client_comments" style="height: 10em" v-model="lead.info.client_comments"></textarea>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="ui styled accordion" style="width: 100%; margin: 20px 0">


        <template v-if="lead.dispositions.length">
            <lead-disposition v-for="(i, d) in lead.dispositions"
                              :id="d.id"
                              :active="false"
                              :disposition="d.type"
                              :by="d.updated_by_user"
                              :contact-date="d.contact_date"
                              :booking-date="d.booking_date | sliceDate"
                              :decline-reasons="d.decline_reasons"
                              :open-reasons="d.open_reasons"
                              :notes="d.notes"
                              :contact-num="i+1"
                              :disabled="!isAdmin"
                              :is-new="false"
            ></lead-disposition>
        </template>


        <lead-disposition v-if="canUpdateDisposition"
                          active="true"
                          :is-new="true"
                          :disposition.sync="newDisposition.type"
                          :booking-date.sync="newDisposition.booking_date"
                          :decline-reasons.sync="newDisposition.decline_reasons"
                          :open-reasons.sync="newDisposition.open_reasons"
                          :notes.sync="newDisposition.notes"
                          :contact-num="lead.dispositions.length + 1"></lead-disposition>


    </div>
    <div class="" style="text-align: right">
        <button  v-if="isAdmin" class="ui red button" @click="deleteCurrentLead()" style="float: left">Delete</button>
        <button class="ui button" @click="closeModal">Close</button>
        <template v-if="canEditCurrentLead() || isAdmin">
            <button class="ui blue button" @click="updateLead()" v-if="lead.info.id"><i class="icon check"></i> Update All</button>
            <button class="ui green button" @click="saveNewLead()" v-if="!lead.info.id"><i class="icon check"></i> Save All</button>
        </template>
    </div>
</div>