<?php require '_header.php' ?>

<script src="/js/reports.js"></script>
<link rel="stylesheet" href="/css/reports.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.3/Chart.bundle.js"></script>

<div class="ui container">

    <div class="ui grid">

        <div class="four wide column">
            <div class="ui segment">
                <h3>Reports</h3>
                <div class="ui form">
                    <div class="field">
                        <label>User <span style="font-weight: 100">(for leads report only)</span></label>
                        <select v-model="report.report_on_user" class="ui dropdown">
                            <option value="all" selected>All Users</option>
                            <option v-for="user in users" :value="user.id">{{ user.name }}</option>
                        </select>
                    </div>

                    <div class="field">
                        <label>Start date</label>
                        <input v-model="report.start_date" class="ui input date">
                    </div>

                    <div class="field">
                        <label>End date</label>
                        <input v-model="report.end_date" class="ui input date">
                    </div>

                    <div class="field">
                        <button @click="runReport()" class="ui blue button"><i class="icon repeat"></i> Conversion</button>
                        <button @click="downloadLeadsReport()" class="ui icon black button"><i class="icon download"></i> Leads</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="twelve wide column">
            <div class="ui segment" v-if="report_results">

                <div class="ui grid">
                    <div class="sixteen wide column">
                        <h3>Individual conversion</h3>
                        <table class="ui  selectable celled striped small table" v-if="report_results">
                            <thead>
                            <tr>
                                <th>User</th>
                                <th>Open</th>
                                <th>Booked</th>
                                <th>Booked Quote</th>
                                <th>Booked Email</th>
                                <th>Booked Upsell</th>
                                <th>Inbound Booked</th>
                                <th>Inbound Declined</th>
                                <th>Declined</th>
                                <th>Invalid</th>
                                <th>Closed</th>
                                <th>Total</th>
                                <th>Conversion</th>
                            </tr>
                            </thead>
                            <tr v-for="user in report_results.byUser">
                                <td>{{ user.name }}</td>
                                <td>{{ user.dispositions.open | parseInt }}</td>
                                <td>{{ user.dispositions.booked | parseInt }}</td>
                                <td>{{ user.dispositions.booked_quote | parseInt }}</td>
                                <td>{{ user.dispositions.booked_email | parseInt }}</td>
                                <td>{{ user.dispositions.booked_upsell | parseInt }}</td>
                                <td>{{ user.dispositions.inbound_booked  | parseInt }}</td>
                                <td>{{ user.dispositions.inbound_declined  | parseInt }}</td>
                                <td>{{ user.dispositions.declined  | parseInt }}</td>
                                <td>{{ user.dispositions.invalid  | parseInt }}</td>
                                <td>{{ user.dispositions.closed  | parseInt }}</td>
                                <td>{{ user.total  | parseInt }}</td>
                                <td :style="backgroundStyle(user.conversion)">{{ user.conversion }}%</td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td>{{ report_results.total.open  | parseInt }}</td>
                                <td>{{ report_results.total.booked  | parseInt }}</td>
                                <td>{{ report_results.total.booked_quote  | parseInt }}</td>
                                <td>{{ report_results.total.booked_email  | parseInt }}</td>
                                <td>{{ report_results.total.booked_upsell  | parseInt }}</td>
                                <td>{{ report_results.total.inbound_booked  | parseInt }}</td>
                                <td>{{ report_results.total.inbound_declined  | parseInt }}</td>
                                <td>{{ report_results.total.declined  | parseInt }}</td>
                                <td>{{ report_results.total.invalid  | parseInt }}</td>
                                <td>{{ report_results.total.closed  | parseInt }}</td>
                                <td>{{ report_results.total.total  | parseInt }}</td>
                                <td :style="backgroundStyle(report_results.total.individual_conversion)">{{ report_results.total.individual_conversion }}%</td>
                            </tr>
                        </table>


                        <h3>Team conversion</h3>
                        <table class="ui  selectable celled striped small table" v-if="report_results">
                            <thead>
                            <tr>
                                <th>Open</th>
                                <th>Booked</th>
                                <th>Booked Quote</th>
                                <th>Booked Email</th>
                                <th>Booked Upsell</th>
                                <th>Inbound Booked</th>
                                <th>Inbound Declined</th>
                                <th>Declined</th>
                                <th>Invalid</th>
                                <th>Closed</th>
                                <th>Total</th>
                                <th>Conversion</th>
                            </tr>
                            </thead>
                            <tr>
                                <td>{{ report_results.total.open | parseInt }}</td>
                                <td>{{ report_results.total.booked | parseInt }}</td>
                                <td>{{ report_results.total.booked_quote | parseInt }}</td>
                                <td>{{ report_results.total.booked_email | parseInt }}</td>
                                <td>{{ report_results.total.booked_upsell | parseInt }}</td>
                                <td>{{ report_results.total.inbound_booked | parseInt }}</td>
                                <td>{{ report_results.total.inbound_declined | parseInt }}</td>
                                <td>{{ report_results.total.declined | parseInt }}</td>
                                <td>{{ report_results.total.invalid | parseInt }}</td>
                                <td>{{ report_results.total.closed | parseInt }}</td>
                                <td>{{ report_results.total.total | parseInt }}</td>
                                <td :style="backgroundStyle(report_results.total.team_conversion)">{{ report_results.total.team_conversion }}%</td>
                            </tr>
                        </table>


                    </div>
                </div>

            </div>
        </div>

    </div>

</div>

<?php require '_footer.php' ?>
