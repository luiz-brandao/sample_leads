<?php require '_header.php' ?>

    <script src="/js/users.js"></script>
    <link rel="stylesheet" href="/css/users.css">

    <div class="ui container">
        <div class="ui centered grid">
            <div class="sixteen wide column">

                <div class="ui two column grid">
                    <div class="column"><h1 class="ui header">Users </h1>
                    </div>
                    <div class="right aligned column">
                        <button class="ui blue button" @click="showNewUser()"><i class="icon add user"></i> New user
                        </button>
                    </div>
                </div>
                <div class="ui segment">

                <table class="ui selectable basic table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(index, user) in users" @click="showEditUser(index)" :class="{ 'user-disabled': !user.is_active }">
                        <td>{{ user.id }}</td>
                        <td>{{ user.name }}</td>
                        <td>{{ user.username }}</td>
                        <td>{{ user.email }}</td>
                        <td>{{ user.is_admin ? "Admin" : "" }}</td>
                    </tr>
                    </tbody>
                </table>
                    </div>
            </div>
        </div>
    </div>


    <div id="edit-user-modal" class="ui small modal" style="max-width: 300px; margin-left: -150px">
        <div class="header">{{ action | capitalize }} User</div>
        <div class="content">
            <form class="ui form">
                <div class="field">
                    <label>Name</label>
                    <input type="text" v-model="editUser.name">
                </div>
                <div class="field">
                    <label>Username</label>
                    <input type="text" v-model="editUser.username">
                </div>
                <div class="field">
                    <label>Email</label>
                    <input type="text" v-model="editUser.email">
                </div>
                <div class="field">
                    <label>Password</label>
                    <input type="password" v-model="editUser.password">
                </div>
                <div class="field">
                    <div class="ui checkbox">
                        <input type="checkbox" v-model="editUser.is_admin">
                        <label>Admin</label>
                    </div>
                </div>
            </form>

        </div>
        <div class="actions">

            <div v-if="editUser.is_active" title="Disable user" class="ui red icon button" style="float: left;" @click="disableUser()"><i class="icon remove user"></i></div>
            <div v-if="!editUser.is_active" title="Enable user" class="ui green icon button" style="float: left;" @click="enableUser()"><i class="icon add user"></i></div>
            <div class="ui cancel button">Cancel</div>
            <button class="ui blue button" type="submit" @click="saveUser()"><i class="icon check"></i>Save</button>
        </div>
    </div>

<?php require '_footer.php' ?>