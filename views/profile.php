<?php require '_header.php' ?>

    <script src="/js/users.js"></script>
    <link rel="stylesheet" href="/css/users.css">

    <div class="ui container">
        <div class="ui grid">
            <div class="row">
                <div class="column">
                    <h1 class="ui header">Profile: <?= $user['name'] ?></h1>
                </div>
            </div>
            <div class="four wide column">


                <div class="ui segment">
                    <h4 class="ui dividing header">Password settings</h4>
                    <div class="ui form">
                        <div class="field">
                            <label>New Password</label>
                            <div class="ui left icon input">
                                <input type="text" placeholder="New password" v-model="newPassword">
                                <i class="lock icon"></i>
                            </div>
                        </div>
                        <div class="field">
                            <button class="ui blue button" type="submit" @click="updatePassword">Update</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


<?php require '_footer.php' ?>