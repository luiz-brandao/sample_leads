<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>Leads Dashboard</title>

    <script src="/js/node_modules/jquery/dist/jquery.js"></script>
    <script src="/js/node_modules/vue/dist/vue.js"></script>
    <script src="/js/node_modules/vue-resource/dist/vue-resource.js"></script>
    <script src="/js/node_modules/datatables.net/js/jquery.dataTables.js"></script>
    <script src="/js/jquery.datetimepicker.full.min.js"></script>
    <script src="/js/semantic.min.js"></script>
    <script src="/js/main.js"></script>

    <link rel="stylesheet" href="/css/semantic.min.css">
    <link rel="stylesheet" href="/css/jquery.datetimepicker.min.css">
    <link rel="stylesheet" href="/css/main.css">

    <link rel="stylesheet" href="/js/node_modules/datatables.net-dt/css/jquery.dataTables.css">
</head>
<body>

<div class="ui top fixed menu">
    <div class="ui container">
        <div class="item">
            <b>EDRY</b>
        </div>
        <a href="/dashboard" class="item"><i class="icon book"></i>Leads</a>
        <a class="item" href="/dashboard/archive"><i class="icon archive"></i> Archive</a>
        <a class="item" href="/dashboard/reports"><i class="icon pie chart"></i> Reports</a>
        <a v-if="user.is_admin" href="/dashboard/users"  class="item"><i class="icon users outline"></i> Users</a>


        <div class="right menu">
            <a class="item" href="/dashboard/profile"><i class="icon user"></i> {{ user.name }}</a>
            <a href="/logout" class="item"><i class="icon arrow right"></i> Logout</a>
            </div>
    </div>
</div>