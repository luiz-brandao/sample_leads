<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>Leads Dashboard</title>

    <script src="../js/node_modules/jquery/dist/jquery.js"></script>
    <script src="../js/node_modules/vue/dist/vue.js"></script>
    <script src="../js/node_modules/vue-resource/dist/vue-resource.js"></script>
    <script src="../js/semantic.min.js"></script>
    <script src="../js/login.js"></script>

    <link rel="stylesheet" href="css/semantic.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/login.css">
</head>
<body style="padding: 0">

<div class="ui middle aligned centered grid">
    <div class="column">
        <h2>Edry Leads Dashboard</h2>
        <form class="ui large form" @submit.prevent="login">
            <div class="ui segment">
                <div class="ui red message" v-if="loginStatus == 'error'"><i class="icon warning circle"></i> Invalid credentials</div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="email" placeholder="Username" v-model="username">
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="password" placeholder="Password" v-model="password">
                    </div>
                </div>
                <button class="ui fluid large blue submit button" type="submit">Login</button>
            </div>

            <div class="ui error message"></div>

        </form>
    </div>
</div>

</body>
</html>