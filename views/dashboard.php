<?php require '_header.php' ?>
    <script>
        // are we displaying the archive or not?
        var isArchive = <?= $isArchive ?>;
    </script>

    <script src="/js/dispositionComponent.js"></script>
    <script src="/js/dashboard.js"></script>
    <link rel="stylesheet" href="/css/dashboard.css">

    <div class="ui container">
        <div class="ui centered stackable grid">

            <div class="three wide column">
                <?php if (!$isArchive): ?>
                    <button class="ui fluid black button" @click="openNewLead()"><i class="icon add"></i> New Lead
                    </button>
                <?php endif; ?>

                <div class="ui vertical fluid menu">

                    <?php if ($isArchive): ?>
                        <div class="header item" style="background: #27292a; color: white;">ARCHIVE</div>
                    <?php endif; ?>

                    <div class="header item">Leads by Disposition</div>


                    <item-filter type="new" color="green">New</item-filter>
                    <item-filter type="open" color="teal">Open</item-filter>
                    <item-filter type="booked" color="blue">Booked</item-filter>
                    <item-filter type="booked_quote" color="violet">Booked Quote</item-filter>
                    <item-filter type="booked_email" color="purple">Booked Email</item-filter>
                    <item-filter type="booked_upsell" color="pink">Booked Upsell</item-filter>
                    <item-filter type="inbound_booked" color="yellow">Inbound Booked</item-filter>
                    <item-filter type="inbound_declined" color="orange">Inbound Declined</item-filter>
                    <item-filter type="declined" color="red">Declined</item-filter>
                    <item-filter type="closed" color="brown">Closed</item-filter>
                    <item-filter type="invalid" color="grey">Invalid</item-filter>


                </div>
            </div>
            <div class="thirteen wide column" style="padding-left: 2em">
                <table id="leads" class="display nowrap" :class="[leadColor]" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Client</th>
                        <th>Post code</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Disposition</th>
                        <th>Processed by</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <div id="lead-modal" class="ui small scrolling modal">
        <div class="content">
            <?php include '_modal.php' ?>
        </div>
    </div>

<?php require '_footer.php' ?>