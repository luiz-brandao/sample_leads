<?php
require 'init.php';

// email account parameters
$HOSTNAME = "electrodry.com.au";
$USERNAME = "sales@electrodry.com.au";
$PASSWORD = "OTMqkfZ.zGt4";

try {
    $client = new Horde_Imap_Client_Socket(array(
        'username' => $USERNAME,
        'password' => $PASSWORD,
        'hostspec' => $HOSTNAME,
        'port' => '993',
        'secure' => 'ssl'
    ));


    // get the UIDs only of the messages that match the specific subject
    $query = new Horde_Imap_Client_Search_Query();
    $query->headerText('SUBJECT', 'Enquiry, sent from yellowpages.com.au');
    $results = $client->search('INBOX', $query);

    // fetch the body of those messages and the date header
    $query = new Horde_Imap_Client_Fetch_Query();
    $query->headers('date', ['Date'], [ 'peek' => true ]);
    $query->bodyText([ 'peek' => true ]);

    // fetch messages
    $uid = new Horde_Imap_Client_Ids($results['match']);
    $messages = $client->fetch('INBOX', $query, [
        'ids' => $uid
    ]);

    // parses each message and add to the database
    foreach ($messages as $m) {
        $parsed = parseYellowPagesEmail($m->getBodyText());

        // add additional fields to the parsed data
        $data = array_merge($parsed, [
            'source' => Edry\Models\Sources::YELLOWPAGES,
            'current_disposition_type' => 'new',
            'created_at' => parseDate($m->getHeaders('date')),
        ]);


        // save new lead
        $db->leads()->createRow($data)->save();
    }

    // move msgs to imported mailbox
    $client->copy('INBOX', 'INBOX.IMPORTED', [
        'ids' => $uid,
        'move' => true
    ]);

} catch (Horde_Imap_Client_Exception $e) {
    error_log($e->getMessage());
}

/**
 * This function parses the email body of a YellowPages email
 * and extract the relevant fields
 *
 * @param $body
 * @return array
 */
function parseYellowPagesEmail($body)
{
    preg_match('/Sender: (.+)/', $body, $matches);
    $sender = $matches[1];

    preg_match('/Phone: (.+)/', $body, $matches);
    $phone = $matches[1];

    preg_match('/Suburb: (.+)/', $body, $matches);
    $address = $matches[1];

    preg_match('/Email Address: (.+)/', $body, $matches);
    $email = $matches[1];

    preg_match('/Comment: (.+?)---/s', $body, $matches);
    $comment = $matches[1];

    // data to be inserted in the database
    $data = [
        'client_name' => $sender,
        'client_phone' => $phone,
        'client_email' => $email,
        'client_comments' => $comment
    ];

    // try to parse the address
    if(preg_match("/(.+?, .+?) ([0-9]+)/", $address, $matches)){
        $data['client_city'] = $matches[1];
        $data['client_postal_code'] = $matches[2];
    } else {
        $data['client_city'] = $address;
    }

    return $data;
}

/**
 * This function extract the date of the email header line
 *
 * @param $dateHeaderLine
 * @return string
 */
function parseDate($dateHeaderLine)
{
    list($headerName, $date) = explode('Date:', $dateHeaderLine);

    $date = new \DateTime(trim($date));
    
    return $date->format('Y-m-d H:i:s')."\n";
}
