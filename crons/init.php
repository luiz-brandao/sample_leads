<?php
date_default_timezone_set('Australia/Sydney');

$loader = require __DIR__.'/../vendor/autoload.php';
$loader->add('Edry\\', __DIR__.'/../');

// loads configuration variables from .env file
$dotenv = new Dotenv\Dotenv('../');
$dotenv->load();

$config = require __DIR__.'/../config.php';

$configDb = $config['db'];
$pdo = new \PDO($configDb['connection_string'], $configDb['username'], $configDb['password']);
$db = new \LessQL\Database($pdo);
