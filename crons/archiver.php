<?php
require 'init.php';

// the amount of days for a lead to be considered old enough to be archived
$ARCHIVE_DAYS = 60;

// archive old leads
$pdo->query("UPDATE leads SET archived = 1 WHERE created_at < NOW() - INTERVAL $ARCHIVE_DAYS DAY");