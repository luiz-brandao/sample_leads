<?php
/*
Plugin Name: Edry Dashboard Connector
Description: This plugin connects the forms to the Leads Dashboard directly, bypassing the email
Version: 1.0.0
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

define( 'DASHBOARD_URL', 'http://edry-env.us-west-2.elasticbeanstalk.com/lead/external' );
define( 'DASHBOARD_SOURCE', 'edry');
define( 'DASHBOARD_PASSWORD', 'somepassword');

/**
 * This function will post the FORM data directly to the Leads Dashboard
 * 
 * @param $postData
 * @return bool|string
 */
function postToDashboard($form)
{
    $ch = curl_init(DASHBOARD_URL);

    $postData = $_POST;

    $postData['password'] = DASHBOARD_PASSWORD;
    $postData['source'] = DASHBOARD_SOURCE;
    $postData['form_id'] = $form->id;

    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postData)
    ));

    // Send the request
    $response = curl_exec($ch);

    // Check for errors
    if ($response === FALSE) {
        return curl_error($ch);
    } else {
        return true;
    }
}

add_action('wpcf7_before_send_mail', 'postToDashboard');