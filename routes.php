<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * All the routes are defined here
 */

/**
 * Unsecured routes (can be accessed without login)
 */
$app->get('/', function (Request $request, Response $response, $args) {
    return $response->withStatus(302)->withHeader('Location', 'dashboard');
});

// login
$app->get('/login', '\Edry\Controllers\SessionController:showLogin')->setName('login');
$app->post('/login', '\Edry\Controllers\SessionController:login')->setName('login');
$app->get('/logout', '\Edry\Controllers\SessionController:logout')->setName('logout');

// endpoint used for integrating with external forms
$app->post('/lead/external', '\Edry\Controllers\LeadsController:createLeadExternal')->setName('lead_external');

/**
 * Secured routes
 */

// dashboard/* routes returns a view
$app->get('/dashboard', '\Edry\Controllers\DashboardController:dashboard');
$app->get('/dashboard/archive', '\Edry\Controllers\DashboardController:archive');
$app->get('/dashboard/users', '\Edry\Controllers\DashboardController:users');
$app->get('/dashboard/profile', '\Edry\Controllers\DashboardController:profile');
$app->get('/dashboard/reports', '\Edry\Controllers\DashboardController:reports');

// all other routes are for AJAX access
$app->get('/session', '\Edry\Controllers\SessionController:getSession');

$app->get('/users', '\Edry\Controllers\UsersController:getUsers');
$app->post('/user', '\Edry\Controllers\UsersController:createUser');
$app->post('/user/{id}/password', '\Edry\Controllers\UsersController:updatePassword');
$app->patch('/user/{id}', '\Edry\Controllers\UsersController:updateUser');
$app->delete('/user', '\Edry\Controllers\UsersController:deleteUser');

$app->get('/leads/overview[/{archive}]', '\Edry\Controllers\LeadsController:getOverview');
$app->get('/leads/{type}[/{archive}]', '\Edry\Controllers\LeadsController:getLeadsByType');
$app->get('/lead/{leadId}', '\Edry\Controllers\LeadsController:getLeadById');
$app->post('/lead', '\Edry\Controllers\LeadsController:createLead');
$app->patch('/lead/{leadId}', '\Edry\Controllers\LeadsController:updateLead');
$app->delete('/lead/{leadId}', '\Edry\Controllers\LeadsController:deleteLead');
$app->get('/lead/unlock/{leadId}', '\Edry\Controllers\LeadsController:unlockLead');

$app->patch('/disposition/{dispositionId}', '\Edry\Controllers\DispositionsController:updateDisposition');

$app->get('/reports', '\Edry\Controllers\ReportsController:conversionReport');
$app->get('/reports/excel', '\Edry\Controllers\ReportsController:excelReport');


