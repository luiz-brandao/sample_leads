<?php
namespace Edry;

/**
 * Class Utils
 *
 * Utility methods
 *
 * @package Edry
 */
class Utils
{

    /**
     * @var array used to translate codes to text
     */
    static $TRANSLATIONS = [
        'new' => 'New',
        'open' => 'Open',
        'booked' => 'Booked',
        'booked_quote' => 'Booked Quote',
        'booked_email' => 'Booked with Email',
        'booked_upsell' => 'Booked with Upsell',
        'inbound_booked' => 'Inbound Booked',
        'inbound_declined' => 'Inbound Declined',
        'declined' => 'Declined',
        'closed' => 'Closed',
        'invalid' => 'Invalid',
        'electrodry' => 'Electrodry',
        'edry' => 'Edry',
        'yellowpages' => 'YellowPages',
        'email_marketing' => 'Email Marketing',
        'live_chat' => 'Live Chat',
        'home_improvement' => 'HI Pages',
        'others' => 'Others',
        'carpet_dry_cleaning' => 'Carpet dry cleaning',
        'upholstery_cleaning' => 'Upholstery cleaning',
        'mattress_cleaning' => 'Mattress cleaning',
        'tile_grout_cleaning' => 'Tile grout cleaning',
        'air_conditioner_cleaning' => 'Air conditioner cleaning',
        'mold_cleaning' => 'Mold cleaning',
        'availability_conflict' => 'Availability conflict',
        'hung_up' => 'Hung up',
        'price_too_high' => 'Price too high',
        'shopping_around' => 'Shopping around',
        'technical_concern' => 'Technical concern',
        'went_to_competitor' => 'Went to competitor',
        'previous_open_lead' => 'Previous open lead',
        'answering_machine' => 'Answering machine',
        'no_answer' => 'No answer',
        'busy' => 'Busy',
        'unavailable' => 'Unavailable',
        'undecided' => 'Undecided'
    ];

    /**
     * Translates a code to a text
     *
     * @param $text
     * @return mixed
     */
    static function translate($text)
    {
        if (key_exists($text, self::$TRANSLATIONS)) {
            return self::$TRANSLATIONS[$text];
        } else {
            return $text;
        }
    }

    /**
     * Translate disposition codes to text 
     * @param $type
     * @return string
     */
    static function translateDisposition($type)
    {
        switch ($type) {
            case 'new':
                return 'New';
            case 'open':
                return 'Open';
            case 'booked':
                return 'Booked';
            case 'booked_quote':
                return 'Booked Quote';
            case 'booked_email':
                return 'Booked with Email';
            case 'booked_upsell':
                return 'Booked with Upsell';
            case 'inbound_booked':
                return 'Inbound Booked';
            case 'inbound_declined':
                return 'Inbound Declined';
            case 'declined':
                return 'Declined';
            case 'closed':
                return 'Closed';
            case 'invalid':
                return 'Invalid';
        }
    }
}