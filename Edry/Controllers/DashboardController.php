<?php
namespace Edry\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Class DashboardController
 *
 * This class is reponsible for returning views associated with each section
 *
 * @package Edry\Controllers
 */
class DashboardController extends BaseController
{
    /**
     * Main leads section
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function dashboard(Request $request, Response $response, $args)
    {
        return $this->ci->view->render($response, 'dashboard.php',[
                'isArchive' => 0
            ]);
    }

    /**
     * Archived leads section
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function archive(Request $request, Response $response, $args)
    {
        return $this->ci->view->render($response, 'dashboard.php', [
            'isArchive' => 1
        ]);
    }

    /**
     * User management section
     *
     * This section is protected and only admins can access it
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return static
     */
    public function users(Request $request, Response $response, $args)
    {
        if (!$this->ci->loggedUser['is_admin']) {
            return $response->withStatus(403);
        }

        return $this->ci->view->render($response, 'users.php');
    }

    /**
     * This section is for users to be able to change their password
     * and other profile details
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function profile(Request $request, Response $response, $args)
    {
        return $this->ci->view->render($response, 'profile.php', [
            'user' => $this->ci->loggedUser
        ]);
    }

    /**
     * This is the Reports section
     * 
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    public function reports(Request $request, Response $response, $args)
    {
        return $this->ci->view->render($response, 'reports.php');
    }


}