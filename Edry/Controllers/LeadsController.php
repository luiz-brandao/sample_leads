<?php
namespace Edry\Controllers;

use Faker\Provider\zh_CN\DateTime;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Edry\Models;

/**
 * Class LeadsController
 *
 * This class is responsible for all CRUD actions related to Leads
 * also locking and unlocking a lead and adding new dispositions
 *
 * @package Edry\Controllers
 */
class LeadsController extends BaseController
{
    /**
     * @var string password for the external link with booking forms from the website
     */
    private $apiPassword = 'somepassword';

    /**
     * Returns the total of leads of each disposition type
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    function getOverview(Request $request, Response $response, $args)
    {
        // from the archive or not?
        $archive = $request->getAttribute('archive') ? 1 : 0;

        // get overview
        $stmt = $this->db->pdo->query("SELECT current_disposition_type, count(*) as total 
                                    FROM leads WHERE archived = $archive GROUP BY current_disposition_type");

        $data = [];
        foreach ($stmt->fetchAll(\PDO::FETCH_ASSOC) as $row) {
            $data[$row['current_disposition_type']] = $row['total'];
        }

        return $response->withJson([
            'overview' => $data
        ]);
    }

    /**
     * Get all leads of a specific disposition type
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    function getLeadsByType(Request $request, Response $response, $args)
    {
        // from the archive or not?
        $archive = $request->getAttribute('archive') ? 1 : 0;

        // uses jQuery DataTables modified class to create a custom search and format the data
        $search = new \Edry\SearchRequest($this->db);
        $results = $search->newFromRequest($request->getQueryParams(), $args['type'], $archive);

        return $response->withJson($results);
    }

    /**
     * Get a lead by ID
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    function getLeadById(Request $request, Response $response, $args)
    {
        $leadId = $args['leadId'];

        $lead = $this->db->leads($leadId);

        // lock the lead so no other user can update it
        if (!$lead->whos_is_editing) {
            $lead->whos_is_editing = $this->ci->loggedUser['id'];
            //$lead->start_editing_time = new \LessQL\Literal('NOW()');
            $lead->save();
        }

        // build list of services (in case all the servers weren't saved or new are added)
        $servicesList = [
            'carpet_dry_cleaning' => null,
            'upholstery_cleaning' => null,
            'mattress_cleaning' => null,
            'tile_grout_cleaning' => null,
            'air_conditioner_cleaning' => null,
            'mold_cleaning' => null,
            'others' => null,
        ];

        $savedServices = json_decode($lead['services_requested'], true);

        if (is_array($savedServices)) {
            $servicesList = array_merge($servicesList, $savedServices);
        }

        $lead->services_requested = $servicesList;

        // get dispositions
        $dispositions = $lead->dispositionsList();

        // @todo make one query only with joins instead of this
        foreach ($dispositions as &$disposition) {
            // get info about the user who created the disposition
            $disposition['updated_by_user'] = $this->db->users($disposition->updated_by_user_id)->name;
        }

        // if there are dipositions and a owner, set the info
        if ($lead->owner_id) {
            $owner = $this->db->users($lead->owner_id);
        } else {
            $owner = null;
        }

        return $response->withJson([
            'lead' => $lead,
            'dispositions' => $dispositions,
            'owner' => $owner
        ]);
    }

    /**
     * Creates a new lead from the dashboard
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    function createLead(Request $request, Response $response, $args)
    {

        $data = $request->getParsedBody();

        $leadData = $data['lead'];
        $newDispositionData = $data['newDisposition'];
        $leadData['services_requested'] = (string)json_encode($leadData['services_requested']);

        $this->db->begin();

        // the front end might send a ID zero, this will interfere with the ORM
        unset($leadData['id']);

        $lead = $this->db->leads()->createRow($leadData);
        $lead->save();

        // @todo this code is mostly the same as the one in the patch method, possible merge
        if (!empty($newDispositionData['type'])) {
            $disposition = $this->db->dispositions()->createRow([
                'leads_id' => $lead['id'],
                'type' => $newDispositionData['type'],
                'contact_date' => new \LessQL\Literal('NOW()'), // @todo maybe get from frontend
                'booking_date' => $newDispositionData['booking_date'],
                'decline_reasons' => $newDispositionData['decline_reasons'],
                'open_reasons' => $newDispositionData['open_reasons'],
                'notes' => $newDispositionData['notes'],
                'updated_by_user_id' => $this->ci->loggedUser['id']
            ])->save();

            $lead->current_disposition_type = $disposition->type;
            $lead->owner_id = $this->ci->loggedUser['id'];

            $lead->save();
        }

        $this->db->commit();

        return $response->withJson([
            'lead' => $leadData
        ]);
    }

    /**
     * Receives an external request to create a new lead
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return static
     */
    function createLeadExternal(Request $request, Response $response, $args)
    {
        $ld = $request->getParsedBody();

        // @todo if needs to check the source
        // if (!in_array($ld['source'], ['edry', 'electrodry-booking', 'electrodry-contact']) || $ld['password'] != $this->apiPassword) {

        // checks the password before continuing
        if ($ld['password'] != $this->apiPassword) {
            // un authorized request
            return $response->withStatus(403);
        }

        // saves a log, for quick check
        // file_put_contents(__DIR__ . '/../../public/log.txt', $request->getBody());

        // identify the source and call the right method to import the data
        if ($ld['source'] == 'electrodry-booking') {
            $this->createLeadFromElectrodyBookingPost($ld);
        } else if ($ld['source'] == 'electrodry-contact') {
            $this->createLeadFromElectrodyContactPost($ld);
        } else if ($ld['source'] == 'edry') {
            // contact form
            if ($ld['form_id'] == 159) {
                $this->createLeadFromEdryContactPost($ld);
            } else if ($ld['form_id'] == 3258) {
                $this->createLeadFromEdryBookingPost($ld);
            }
        }
    }

    /**
     * Creates a lead with data from Electrodry Booking form
     *
     * @param $data
     */
    function createLeadFromElectrodyBookingPost($data)
    {
        $this->db->begin();

        $lead = $this->db->leads()->createRow([]);
        $lead->source = 'electrodry';
        $lead->current_disposition_type = 'new';
        $lead->client_name = $data['fullname'];
        $lead->client_email = $data['contact_email'];
        $lead->client_phone = $data['contact_phone'];
        $lead->client_mobile_phone = $data['contact_mobile'];
        $lead->client_address = $data['address'];
        $lead->client_city = $data['city'];
        $lead->client_postal_code = $data['postcode'];

        $date = \DateTime::createFromFormat('Y-m-d', $data['service_date']);

        if($date){
            $lead->client_service_date = $date->format('Y-m-d');
        }

        $servicesList = [
            'carpet_dry_cleaning' => (array_search('Carpet Dry Cleaning', $data['Services']) !== false),
            'upholstery_cleaning' => (array_search('Upholstery Cleaning', $data['Services']) !== false),
            'mattress_cleaning' => (array_search('Mattress Cleaning', $data['Services']) !== false),
            'tile_grout_cleaning' => (array_search('Tile & Grout Cleaning', $data['Services']) !== false),
            'air_conditioner_cleaning' => (array_search('Air Conditioner Cleaning', $data['Services']) !== false),
            'mold_cleaning' => (array_search('Mold Cleaning', $data['Services']) !== false),
            'others' => false,
        ];

        $lead->services_requested = json_encode($servicesList);

        if (isset($data['add_deals'])) {
            $deals = implode("\n\n", $data['add_deals']);
            $deals = strip_tags($deals);
        } else {
            $deals = '';
        }

        $lead->client_comments = $data['comment_details'] . "\n\nDeals:\n" . $deals;

        $lead->save();
        $this->db->commit();
    }

    /**
     * Creates a lead with data from Electrodry Contactin form
     *
     * @param $data
     */
    function createLeadFromElectrodyContactPost($data)
    {
        $this->db->begin();

        $lead = $this->db->leads()->createRow([]);
        $lead->source = 'electrodry';
        $lead->current_disposition_type = 'new';
        $lead->client_name = $data['fullname'];
        $lead->client_email = $data['email'];
        $lead->client_phone = $data['phone'];
        $lead->client_postal_code = $data['postcode'];

        $lead->client_comments = $data['message'];

        $lead->save();
        $this->db->commit();
    }

    /**
     * Creates a lead from Edry Contact form
     *
     * @param $data
     */
    function createLeadFromEdryContactPost($data)
    {
        $this->db->begin();

        $lead = $this->db->leads()->createRow([]);
        $lead->source = 'edry';
        $lead->current_disposition_type = 'new';
        $lead->client_name = $data['your-name'];
        $lead->client_email = $data['your-email'];
        $lead->client_phone = $data['Phone'];
        $lead->client_postal_code = $data['postcode'];
        $lead->client_comments = $data['your-message'];

        $lead->save();
        $this->db->commit();
    }

    /**
     * Creates a lead from Edry booking form
     * @param $data
     */
    function createLeadFromEdryBookingPost($data)
    {
        $this->db->begin();

        $lead = $this->db->leads()->createRow([]);
        $lead->source = 'edry';
        $lead->current_disposition_type = 'new';
        $lead->client_name = $data['your-name'];
        $lead->client_email = $data['your-email'];
        $lead->client_phone = $data['Phone'];
        $lead->client_mobile_phone = $data['your-mobile'];
        $lead->client_address = $data['address'];
        $lead->client_city = $data['city'];
        $lead->client_postal_code = $data['postcode'];

        $date = \DateTime::createFromFormat('m/d/Y', $data['Whenwouldyoulikeourservice']);

        if($date){
            $lead->client_service_date = $date->format('Y-m-d');
        }

        $servicesList = [
            'carpet_dry_cleaning' => (array_search('Carpet Dry Cleaning', $data['Services']) !== false),
            'upholstery_cleaning' => (array_search('Upholstery Cleaning', $data['Services']) !== false),
            'mattress_cleaning' => (array_search('Mattress Cleaning', $data['Services']) !== false),
            'tile_grout_cleaning' => (array_search('Tile & Grout Cleaning', $data['Services']) !== false),
            'air_conditioner_cleaning' => (array_search('Air Conditioner Cleaning', $data['Services']) !== false),
            'mold_cleaning' => (array_search('Mold Cleaning', $data['Services']) !== false),
            'others' => false,
        ];

        $lead->services_requested = json_encode($servicesList);

        if (isset($data['add_deals'])) {
            $deals = implode("\n\n", $data['add_deals']);
            $deals = strip_tags($deals);
        } else {
            $deals = '';
        }
        
        $lead->client_comments = $data['your-message'] . "\n\nDeals:\n" . $deals;

        $lead->save();
        $this->db->commit();
    }

    /**
     * Updates a lead from the dashboard
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    function updateLead(Request $request, Response $response, $args)
    {

        $data = $request->getParsedBody();

        $leadData = $data['lead'];
        $newDispositionData = $data['newDisposition'];

        $leadData['services_requested'] = (string)json_encode($leadData['services_requested']);

        $this->db->begin();

        // @todo check if is admin or who_is_editing matches the logged user
        // right now it relies on the front end to block users from editing a lead
        $lead = $this->db->leads($leadData['id'])->update($leadData);

        // @todo this code is mostly the same the one in the post method, possible merge
        if (!empty($newDispositionData['type'])) {
            $disposition = $this->db->dispositions()->createRow([
                'leads_id' => $lead['id'],
                'type' => $newDispositionData['type'],
                'contact_date' => new \LessQL\Literal('NOW()'), // @todo maybe get from frontend
                'booking_date' => $newDispositionData['booking_date'],
                'decline_reasons' => $newDispositionData['decline_reasons'],
                'open_reasons' => $newDispositionData['open_reasons'],
                'notes' => $newDispositionData['notes'],
                'updated_by_user_id' => $this->ci->loggedUser['id']
            ])->save();

            $lead->current_disposition_type = $disposition->type;
            $lead->last_update = new \LessQL\Literal('NOW()');
            $lead->owner_id = $this->ci->loggedUser['id'];
            $lead->save();
        }

        $this->db->commit();

        return $response->withJson([
            'lead' => $leadData
        ]);
    }

    /**
     * Deletes a lead
     * 
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return static
     */
    function deleteLead(Request $request, Response $response, $args){
        if (!$this->ci->loggedUser['is_admin']) {
            return $response->withStatus(403);
        }

        $this->db->leads( $args['leadId'])->delete();
    }

    /**
     * Unlocks a lead
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    function unlockLead(Request $request, Response $response, $args)
    {
        $leadId = $args['leadId'];

        $lead = $this->db->leads($leadId);

        if ($lead->whos_is_editing == $this->ci->loggedUser['id']) {
            $lead->whos_is_editing = null;
            $lead->save();
        }

    }
}