<?php
namespace Edry\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Class UsersController
 *
 * This class is responsible for Users CRUD
 *
 * @package Edry\Controllers
 */
class UsersController extends BaseController
{
    /**
     * Gets list of users
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    function getUsers(Request $request, Response $response, $args) {
        $users = $this->db->users();
        $users->fetchAll();

        foreach ($users as &$user){
            $user['is_admin'] = (bool)$user['is_admin'];
            $user['is_active'] = (bool)$user['is_active'];
            unset($user['password']);
        }

        return $response->withJson($users);
    }

    /**
     * Creates new user (only for admins)
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return bool
     */
    function createUser(Request $request, Response $response, $args) {

        if (!$this->ci->loggedUser['is_admin']) {
            return false;
        }

        $data = $request->getParsedBody();

        $newUser = [
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'is_admin' => (int)$data['is_admin'],
            'password' => password_hash($data['password'], PASSWORD_DEFAULT)
        ];

        try {
            $user = $this->db->users()->createRow($newUser)->save();

            return $response->withJson($user->id);
        } catch (Exception $e) {
            return $response->withStatus(400)->withJson($e);
        }
    }

    /**
     * Updates user (only for admins)
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return bool|static
     */
    function updateUser(Request $request, Response $response, $args) {

        if (!$this->ci->loggedUser['is_admin']) {
            return false;
        }

        $data = $request->getParsedBody();

        $user = [
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'is_admin' => (bool)$data['is_admin'],
            'is_active' => (bool)$data['is_active'],
        ];

        if(!empty($data['password'])){
            $user['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        }

        try {
            $user = $this->db->users($args['id'])->update($user)->save();

            return $response->withStatus(200);

        } catch (Exception $e) {
            return $response->withStatus(400)->withJson($e);
        }

    }

    /**
     * Updates user password
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return static
     */
    function updatePassword(Request $request, Response $response, $args){
        $data = $request->getParsedBody();

        $userId = $args['id'];

        try {
            $user = $this->db->users($userId)->update([
                'password' => password_hash($data['password'], PASSWORD_DEFAULT)
            ])->save();

            return $response->withStatus(200);

        } catch (Exception $e) {
            return $response->withStatus(400)->withJson($e);
        }
    }

    /**
     * Deletes an user (deactivate user)
     * 
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return bool|static
     */
    function deleteUser(Request $request, Response $response, $args) {
        if (!$this->ci->loggedUser['is_admin']) {
            return false;
        }

        $user = $this->db->users($args['id']);
        $user->is_active = false;
        $user->password = '';
        $user->save();

        return $response->withStatus(200);
    }
    
    
}