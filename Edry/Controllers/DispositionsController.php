<?php
namespace Edry\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Class DispositionsController
 *
 * This class is responsible for some actions related to dispositions.
 * All actions are accessed through AJAX and data is sent as JSON.
 *
 * !! New dispositions are created on the LeadsController when updating a lead
 *
 * @package Edry\Controllers
 */
class DispositionsController extends BaseController
{
    /**
     * Updates a disposition of a lead
     *
     * It updates the disposition and checks if it's the most recent one.
     * If it's the most recent one, also update the lead.
     * This is needed because a disposition can be rectified even if it's not the newest one.
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    function updateDisposition(Request $request, Response $response, $args){
        $data = $request->getParsedBody();

        $this->db->begin();

        // loads the disposition by ID and update fields
        $newDisposition = $this->db->dispositions( $args['dispositionId'])->update($data['disposition']);

        // get lead to check if should update current disposition state
        $lead = $this->db->leads($newDisposition->leads_id);

        // check if it's the last disposition based on the highest ID
        $shouldUpdate = true;
        foreach ($lead->dispositionsList() as $disposition){
            if($disposition->id > $newDisposition->id){
                $shouldUpdate = false;
                break;
            }
        };

        // updates the lead if necessary
        if($shouldUpdate){
            $lead->current_disposition_type = $newDisposition->type;
            $lead->save();
        }

        $this->db->commit();

        return $response->withJson([
            'disposition' => $newDisposition
        ]);
    }
}
