<?php
namespace Edry\Controllers;

use Edry\Utils;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Class ReportsController
 *
 * This class is responsible for all reporting actions
 *
 * @package Edry\Controllers
 */
class ReportsController extends BaseController
{

    /**
     * Generates an excel report
     *
     * @param Request $request
     * @param Response $response
     * @return Response|static
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    function excelReport(Request $request, Response $response)
    {
        $data = $request->getQueryParams();

        $pdo = $this->db->pdo;

        $startDate = $pdo->quote((new \DateTime($data['start_date']))->format('Y-m-d'));
        $endDate = $pdo->quote((new \DateTime($data['end_date']))->format('Y-m-d'));

        // sets title and query parameters depending on the type of report
        if ($data['report_on_user'] == 'all') {
            $userWhere = '';
            $reportTitle = "Team Report";
        } else {
            $ownerId = $pdo->quote($data['report_on_user']);
            $userWhere = " AND owner_id = $ownerId ";

            $user = $this->db->users($data['report_on_user']);
            $reportTitle = $user->name;
        }

        // start building the excell file
        $excel = new \PHPExcel();
        $excel->setActiveSheetIndex(0);
        $sheet = $excel->getActiveSheet();

        // info
        $sheet->mergeCells('A1:B1');
        $sheet->setCellValueByColumnAndRow(0, 1, $reportTitle);
        $sheet->setCellValueByColumnAndRow(2, 1, $data['start_date']);
        $sheet->setCellValueByColumnAndRow(3, 1, $data['end_date']);

        $sheet->getStyle("A1:D1")->getFont()->setBold(true)->setSize(12);

        $rowN = 2;
        $colN = 0;

        $headers = [
            'ID',
            'Source',
            'Disposition',
            'Owner',
            'Created at',
            'Services',

            'Client Name',
            'Client Email',
            'Client Phone',
            'Client Mobile',
            'Client Address',
            'Client City',
            'Client Postcode',
            'Client Service date',
            'Client Comments',

            'Contact #1 Disposition',
            'Contact #1 Contact date',
            'Contact #1 Booking date',
            'Contact #1 Decline reason',
            'Contact #1 Open reason',
            'Contact #1 Notes',
            'Contact #1 User',
            'Contact #1 Created at',

            'Contact #2 Disposition',
            'Contact #2 Contact date',
            'Contact #2 Booking date',
            'Contact #2 Decline reason',
            'Contact #2 Open reason',
            'Contact #2 Notes',
            'Contact #2 User',
            'Contact #2 Created at',

            'Contact #3 Disposition',
            'Contact #3 Contact date',
            'Contact #3 Booking date',
            'Contact #3 Decline reason',
            'Contact #3 Open reason',
            'Contact #3 Notes',
            'Contact #3 User',
            'Contact #3 Created at'
        ];

        foreach ($headers as $header) {
            $sheet->setCellValueByColumnAndRow($colN, $rowN, $header);
            $colN++;
        }

        $sheet->getStyle("A2:AM2")->getFont()->setBold(true);

        $rowN++;
        $colN = 0;

        // the leads report query
        $lSql = "SELECT l.id, l.source, l.current_disposition_type, u.name, l.created_at, l.services_requested, 
                    l.client_name, l.client_email, l.client_phone, l.client_mobile_phone, l.client_address, l.client_city, l.client_postal_code, 
                    l.client_service_date, l.client_comments
                    FROM leads l, users u WHERE l.owner_id = u.id AND CAST(l.created_at as DATE) between DATE($startDate) and DATE($endDate) $userWhere";

        $resLeads = $pdo->query($lSql);

        while ($lead = $resLeads->fetch(\PDO::FETCH_NUM)) {
            $lead[5] = $this->reformatServices($lead[5]);

            // add lead info to the excel file
            foreach ($lead as $value) {
                $sheet->setCellValueByColumnAndRow($colN, $rowN, Utils::translate($value));
                $colN++;
            }

            // disposition query
            $dSql = "SELECT d.type, d.contact_date, d.booking_date, d.decline_reasons, d.open_reasons, d.notes, u.name, d.created_at
                      FROM dispositions d, users u WHERE  d.updated_by_user_id = u.id AND d.leads_id = {$lead[0]}";

            $resDisp = $pdo->query($dSql);

            while ($disposition = $resDisp->fetch(\PDO::FETCH_NUM)) {

                // add disposition to excel file
                foreach ($disposition as $value) {
                    $sheet->setCellValueByColumnAndRow($colN, $rowN, Utils::translate($value));
                    $colN++;
                }

            }

            $colN = 0;
            $rowN++;
        }

        for ($col = 0; $col < 41; $col++) {
            $sheet->getColumnDimensionByColumn($col)->setAutoSize(true);
        }

        // filename
        $filename = str_ireplace(' ', '_', "$reportTitle {$data['start_date']} {$data['end_date']}");

        // generate file
        $write = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');

        // write it to the output to be downloaded
        $response = $response->withHeader('Content-type', 'application/octet-stream');
        $response = $response->withHeader('Content-Disposition', "attachment; filename=$filename.xlsx");
        $response = $response->withHeader('Cache-Control', "max-age=0");
        $response = $response->withHeader('Content-Transfer-Encoding', "binary");

        $write->save('php://output');

        return $response;
    }

    /**
     * This method formats the list of services to fit in one line
     *
     * @param $servicesJson
     * @return string
     */
    private function reformatServices($servicesJson)
    {
        $services = [];

        foreach (json_decode($servicesJson) as $service => $status) {
            if (!$status) {
                $services[] = $service;
            }
        }

        array_walk($services, function (&$value) {
            $value = Utils::translate($value);
        });

        return implode(', ', $services);
    }

    /**
     * Generates conversion reports
     *
     * @param Request $request
     * @param Response $response
     * @return mixed
     */
    function conversionReport(Request $request, Response $response)
    {
        $data = $request->getQueryParams();

        $pdo = $this->db->pdo;

        $startDate = $pdo->quote((new \DateTime($data['start_date']))->format('Y-m-d'));
        $endDate = $pdo->quote((new \DateTime($data['end_date']))->format('Y-m-d'));

        // leads report query
        $sql = "SELECT  count(*) as total, 
                        l.current_disposition_type as disposition, 
                        l.owner_id as owner, 
                        u.name
                FROM leads l JOIN users u ON (u.id = l.owner_id)
                WHERE 
                    CAST(l.created_at as DATE) between $startDate and $endDate
                GROUP BY owner, disposition;";

        $results = $pdo->query($sql)->fetchAll(\PDO::FETCH_OBJ);


        // reformat code
        $byUser = [];

        // organize dispositions by user
        foreach ($results as $result) {
            $byUser[$result->owner]['dispositions'][$result->disposition] = $result->total;
            $byUser[$result->owner]['name'] = $result->name;
        }

        // totals by user
        foreach ($byUser as &$user) {
            list($conversion, $booked, $total) = $this->calculateIndividualConversionAndSubTotals($user['dispositions']);

            $user['total'] = $total;
            $user['dispositions']['all_booked'] = $booked;
            $user['conversion'] = $conversion;
        }

        unset($user);

        $grandTotal = [
            'all_booked' => 0,
            'open' => 0,
            'declined' => 0,
            'closed' => 0,
            'total' => 0,
        ];

        // calculate grand totals
        foreach ($byUser as $user) {
            foreach ($user['dispositions'] as $index => $value) {
                @$grandTotal[$index] += $value;
            }

            $grandTotal['total'] += @$user['total'];
        }

        $totalIndividualConversion = $this->calculateIndividualConversionAndSubTotals($grandTotal)[0];
        $teamConversion = $this->calculateTeamConversion($grandTotal);

        $grandTotal['individual_conversion'] = $totalIndividualConversion;
        $grandTotal['team_conversion'] = $teamConversion;


        return $response->withJson(['byUser' => $byUser, 'total' => $grandTotal]);

    }

    /**
     * Given an array of dispositions, calculate the individual conversion
     *
     * @param $d
     * @return array
     */
    private function calculateIndividualConversionAndSubTotals($d)
    {
        if (isset($d['all_booked'])) {
            $booked = $d['all_booked'];
        } else {
            $booked = @$d['booked'] + @$d['booked_email'] + @$d['booked_upsell'];
        }

        $others = @$d['declined'] + @$d['open'] + @$d['closed'];

        $bookedAndOthers = $booked + $others;

        // make sure not to divide by zero
        if ($bookedAndOthers) {
            $conversion = $booked / $bookedAndOthers;
        } else {
            $conversion = 0;
        }

        return [$this->formatConversionValue($conversion), $booked, $bookedAndOthers];
    }

    /**
     * Properly format conversion value
     *
     * @param $value
     * @return string
     */
    private function formatConversionValue($value)
    {
        return number_format((round($value, 4) * 100), 2, '.', '');
    }

    /**
     * Given an array of dispositions totals, calculate the total team conversion
     *
     * @param $totals
     * @return string
     */
    private function calculateTeamConversion($totals)
    {
        $booked = $totals['booked'] + $totals['booked_quote'] + $totals['booked_email'] + $totals['booked_upsell'] + $totals['inbound_booked'];
        $others = $totals['declined'] + $totals['open'] + $totals['closed'] + $totals['inbound_declined'];
        $bookedAndOthers = $booked + $others;

        // make sure not to divide by zero
        if ($bookedAndOthers) {
            $conversion = $booked / $bookedAndOthers;
        } else {
            $conversion = 0;
        }

        return $this->formatConversionValue($conversion);
    }

}