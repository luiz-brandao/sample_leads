<?php
namespace Edry\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Class SessionController
 *
 * This class is responsible for login and logouts
 *
 * @package Edry\Controllers
 */
class SessionController extends BaseController
{
    /**
     * Return information about the session
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    function getSession(Request $request, Response $response, $args)
    {
        $user = $this->ci->loggedUser;
        return $response->withJson($user);
    }

    /**
     * Displays the login view
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    function showLogin(Request $request, Response $response, $args)
    {
        return $this->ci->view->render($response, 'login.php');
    }


    /**
     * Process user login
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return mixed
     */
    function login(Request $request, Response $response, $args)
    {
        $params = $request->getParsedBody();

        $user = \Edry\Models\Users::validate($this->db, $params);

        if ($user) {
            \Edry\Models\Users::registerSession($user);

            // unlock leads (in case previous leads didn't get unlocked properly)
            $userId = $this->ci->loggedUser['id'];
            $this->db->pdo->query("UPDATE leads SET whos_is_editing = null WHERE whos_is_editing = $userId");

            return $response->withJson(['status' => 'logged']);
        } else {
            return $response->withStatus(403)->withJson(['status' => 'invalid_user']);
        }

    }

    /**
     * Logs user out
     * 
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return static
     */
    function logout(Request $request, Response $response, $args)
    {
        // unlock leads
        $userId = $this->ci->loggedUser['id'];
        $this->db->pdo->query("UPDATE leads SET whos_is_editing = null WHERE whos_is_editing = $userId");

        $_SESSION = [];
        return $response->withStatus(302)->withHeader('Location', 'dashboard');
    }
}