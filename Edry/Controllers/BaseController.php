<?php
namespace Edry\Controllers;

use \Interop\Container\ContainerInterface;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Class BaseController
 * 
 * Use by all the other controllers, this simply makes accessing 
 * the Container and the database ORM easier
 * 
 * @package Edry\Controllers
 */
class BaseController
{
    protected $ci;

    /**
     * @var LessQL
     */
    protected $db;

    public function __construct(ContainerInterface $ci) {
        $this->ci = $ci;
        $this->db = $ci->db;
    }

}