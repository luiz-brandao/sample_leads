<?php
namespace Edry\Models;

/**
 * Class Dispositions
 * 
 * List of disposition codes for easy access
 * 
 * @package Edry\Models
 */
class Dispositions
{
    const NEW_LEAD = 'NEW';
    const BOOKED = 'BOOKED';
    const BOOKED_QUOTE = 'BOOKED_QUOTE';
    const BOOKED_EMAIL = 'BOOKED_EMAIL';
    const BOOKED_UPSELL = 'BOOKED_UPSELL';
    const DECLINED = 'DECLINED';
    const OPEN = 'OPEN';
    const INBOUND_BOOKED = 'INBOUND_BOOKED';
    const INBOUND_DECLINED = 'INBOUND_DECLINED';
    const CLOSED = 'CLOSED';
    const INVALID = 'INVALID';


}
