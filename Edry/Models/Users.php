<?php
namespace Edry\Models;

/**
 * Class Users
 *
 * This model is reponsible for validate users and registering the session
 *
 * @package Edry\Models
 */
class Users {

    /**
     * Check user credentials
     *
     * @param $db
     * @param $credentials
     * @return bool
     */
    static function validate($db, $credentials){
        $user = $db->users()->where('username', $credentials['username'])->fetch();

        if($user){
            if(password_verify($credentials['password'], $user->password)){
                $user['is_admin'] = (bool)$user['is_admin'];
                return $user;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Register user session
     * 
     * @param $user
     */
    static function registerSession($user){
        unset($user['password']);

        $_SESSION['user'] = $user->jsonSerialize();
    }
    
}