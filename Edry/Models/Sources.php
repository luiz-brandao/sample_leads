<?php
namespace Edry\Models;

/**
 * Class Sources
 * 
 * List of source codes for easy access
 * 
 * @package Edry\Models
 */
class Sources
{
    const ELECTRODRY = 'electrodry';
    const EDRY = 'edry';
    const YELLOWPAGES = 'yellowpages';
    const EMAIL_MARKETING = 'email_marketing';
    const LIVE_CHAT = 'live_chat';
    const OTHERS = 'others';
}