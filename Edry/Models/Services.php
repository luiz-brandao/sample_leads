<?php
namespace Edry\Models;

/**
 * Class Services
 * 
 * List of service codes for easy access
 * 
 * @package Edry\Models
 */
class Services
{
    const CARPET_DRY_CLEANING = 'carpet_dry_cleaning';
    const UPHOLSTERY_CLEANING = 'upholstery_cleaning';
    const MATTRESS_CLEANING = 'mattress_cleaning';
    const TILE_GROUT_CLEANING = 'tile_grout_cleaning';
    const AIR_CONDITIONER_CLEANING = 'air_conditioner_cleaning';
    const MOLD_CLEANING = 'mold_cleaning';
    const OTHERS = 'others';
}