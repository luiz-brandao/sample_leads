<?php
namespace Edry;

/**
 * Class SearchRequest
 *
 * This is a modified version of the sample class provided by jQuery DataTables
 *
 * This class is responsible for issuing a query and formatting the result
 * according to the format expected by DataTables
 *
 * @package Edry
 */
class SearchRequest
{

    /**
     * @var ORM instance
     */
    protected $db;

    /**
     * @var string name of the table to be queried
     */
    protected $table = 'view1';

    /**
     * @var string primary key column
     */
    protected $primaryKey = 'lead_id';

    /**
     * @var list of columns
     */
    protected $columns;


    /**
     * SearchRequest constructor.
     */
    public function __construct($db)
    {
        $this->db = $db;
        $this->setupCols();

    }

    /**
     * Maps db columns to datatables columns
     */
    private function setupCols(){
        $this->columns = [
            ['db' => 'created_at', 'dt' => 0, 'formatter' => function ($d, $row) { return date('M jS, H:i', strtotime($d)); }],
            ['db' => 'client_name', 'dt' => 1],
            ['db' => 'client_postal_code', 'dt' => 2],
            ['db' => 'client_phone', 'dt' => 3],
            ['db' => 'client_email', 'dt' => 4],
            ['db' => 'current_disposition_type', 'dt' => 5],
            ['db' => 'owner_name', 'dt' => 6],
        ];
    }

    /**
     * Triggers the search
     *
     * @param array $request
     * @param string $disposition
     * @param int $archive
     * @return array
     */
    public function newFromRequest(array $request, $disposition = 'new', $archive = 0)
    {
        // custom query parameters
        $where = "archived = $archive AND current_disposition_type = " . $this->db->pdo->quote($disposition);

        // query
        return SSP::complex($request, $this->db->pdo, $this->table, $this->primaryKey, $this->columns, $where);

    }

}



