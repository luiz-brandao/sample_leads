<?php
/**
 * All the services are defined and added to the container here
 */
$container = $app->getContainer();

/**
 * Initialize LessQL ORM
 *
 * For more information,
 * http://http://lessql.net/
 *
 * @param $container
 * @return \LessQL\Database
 */
$container['db'] = function ($container) {
    $config = $container['settings']['db'];
    $pdo = new \PDO($config['connection_string'], $config['username'], $config['password'], [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET time_zone = '+10:00'"
    ]);
    return new \LessQL\Database($pdo);
};

// setup the PHP view renderer
$container['view'] = function ($container) {
    return new \Slim\Views\PhpRenderer(APP_PATH.'/views/');
};