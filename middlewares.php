<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Permanently redirect paths with a trailing slash to their non-trailing counterpart
 */
$app->add(function (Request $request, Response $response, callable $next) {
    $uri = $request->getUri();
    $path = $uri->getPath();
    if ($path != '/' && substr($path, -1) == '/') {
        $uri = $uri->withPath(substr($path, 0, -1));
        return $response->withRedirect((string)$uri, 301);
    }

    return $next($request, $response);
});

/**
 * Checks if the user is logged and blocks access if not
 */
$app->add(function (Request $request, Response $response, callable $next) {

    /**
     * @var $route Slim\Route
     */
    $route = $request->getAttribute('route');

    // if the session doesn't exist, redirect to login url unless is one of the unprotected routes
    if (!isset($_SESSION['user']) && $route->getName() !== 'login' && $route->getName() !== 'lead_external') {
        return $response->withStatus(302)->withHeader('Location', LOGIN_URL);
    }
    
    // load user info
    if (isset($_SESSION['user'])) {
        $this['loggedUser'] = $_SESSION['user'];
    }

    $response = $next($request, $response);

    return $response;
});


